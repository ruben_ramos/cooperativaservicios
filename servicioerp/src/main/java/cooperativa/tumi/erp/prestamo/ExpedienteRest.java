package cooperativa.tumi.erp.prestamo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cooperativa.tumi.servicio.Expediente;


@RequestMapping("expediente")
@RestController
public class ExpedienteRest {
	
	@GetMapping("anulado")
	public Expediente buscar() {
		Expediente expediente=new Expediente();
		expediente.setIdPersona((int)(Math.random()*1000));
		expediente.setIdCuenta((int)(Math.random()*1000));
		expediente.setIdExpedinte((int)(Math.random()*1000));
		expediente.setIdExpedienteDetalle((int)(Math.random()*1000));
		return expediente;
	}
}
