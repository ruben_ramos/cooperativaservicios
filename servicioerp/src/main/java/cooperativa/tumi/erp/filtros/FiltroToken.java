package cooperativa.tumi.erp.filtros;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter(urlPatterns="/rest/*")
public class FiltroToken implements Filter{

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpServletRequest=(HttpServletRequest)request;
		HttpServletResponse httpServletResponse=(HttpServletResponse)response;
		
		System.out.println("CONTEXTO : "+httpServletRequest.getContextPath());
		System.out.println("token cliente : "+httpServletRequest.getHeader("token"));
		System.out.println("otros cliente : "+httpServletRequest.getHeader("otros"));
		System.out.println("Host cliente : "+httpServletRequest.getHeader("Host"));
		System.out.println("remote addr : "+httpServletRequest.getRemoteAddr());
		System.out.println("remote host : "+httpServletRequest.getRemoteHost());
		int token=(int)(Math.random()*100000);
		
		System.out.println("token inicio: "+token);
		httpServletResponse.setHeader("token", ""+token);
		httpServletResponse.setHeader("Server", "Apache X");
		
		chain.doFilter(request, response);
		
		System.out.println("token fin: "+httpServletResponse.getHeader("token"));
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
